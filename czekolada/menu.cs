using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace czekolada
{
    public partial class Menu : Form
    {
        List<ChocoShop> list = new List<ChocoShop>();
        Chocolate choco = new Chocolate("Milka", "Milka", 100, 2.85, "mleczna");
        Chocolate choco2 = new Chocolate("Oryginalna mleczna", "Goplana", 100, 2.20, "mleczna");
        CandyBar bar = new CandyBar("Twix", "Wedel", 30, 1.30, "ciastko+karmel+czekolada");
        CandyBar bar2 = new CandyBar("Mars", "Masterfoods Polska", 35, 1.90, "nugat + mleczna czekolada + karmel");
        Other ot = new Other("Bananki czekoladowe", "Goplana", 250, 11.99, 20);
        Other ot2 = new Other("Ptasie mleczko", "Wedel", 300, 13.99, 40);
    
        public Menu()
        {
            InitializeComponent();
            list.Add(choco);
            list.Add(choco2);
            list.Add(bar);
            list.Add(bar2);
            list.Add(ot);
            list.Add(ot2);
            foreach(ChocoProducts product in list)
            {
                listBox1.Items.Add(product.getName());
            }
            comboBox1.SelectedIndex = 3;
        }

        private void AddItemCallback2(ChocoProducts product)
        {
            list.Add(product);
            listBox1.Items.Add(product.getName());
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            Add okno = new Add();
            okno.AddItemCallback += new Add.AddChoco(this.AddItemCallback2);
            okno.Show();
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            int index = listBox1.SelectedIndex;
            ChocoProducts choco = (ChocoProducts)list[index];
            Edit okno1 = new Edit(index,choco);
            okno1.EditItemCallback += new Edit.EditChoco(this.okno1_EditItemCallback);
            okno1.Show();
        }

        void okno1_EditItemCallback(int index, ChocoProducts product)
        {
            list[index] = product;
            listBox1.Items.Clear();
            foreach (ChocoProducts c in list)
            {
                listBox1.Items.Add(c.getName());
            }

        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            int index = listBox1.SelectedIndex;
            if (index >= 0)
            {
                list.RemoveAt(index);
                listBox1.Items.Clear();
                foreach (ChocoProducts c in list)
                {
                    listBox1.Items.Add(c.getName());
                }
            }
            else
            {
                MessageBox.Show("Wybierz element z listy");
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = listBox1.SelectedIndex;
            if (index >= 0)
            {
                buttonEdit.Enabled = true;
                textBoxProducer.Text = list[index].getProducer();
                textBoxPrice.Text = list[index].getPrice().ToString();
                textBoxWeight.Text = list[index].getWeight().ToString();
                if (list[index] is Chocolate)
                {
                    Chocolate choco = (Chocolate)list[index];
                    textBoxKind.Text = choco.getKind();
                    textBoxPiecesInPack.Text = "";
                    textBoxDescription.Text = "";
                }
                else
                {
                    if (list[index] is CandyBar)
                    {
                        CandyBar bar = (CandyBar)list[index];
                        textBoxDescription.Text = bar.getDescription();
                        textBoxPiecesInPack.Text = "";
                        textBoxKind.Text = "";
                    }
                    else
                    {
                        Other ot = (Other)list[index];
                        textBoxPiecesInPack.Text = ot.getPiecesInPack().ToString();
                        textBoxKind.Text = "";
                        textBoxDescription.Text = "";
                    }
                }
                
            }
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            string path = Application.StartupPath;
            string MyPath = @path + "\\dane\\choco.txt";
            DirectoryInfo dir = new DirectoryInfo(@path + "\\dane");
            FileInfo file = new FileInfo(@path + "\\dane\\choco.txt");
            if (dir.Exists == true) //sprawdzenie czy plik i katalog istniej�
            {
                if (file.Exists == false)
                {
                    file.Create();
                }
            }
            else
            {
                dir.Create();
            }
            FileStream FileW = new FileStream(MyPath, FileMode.Truncate); //otworzenie oraz wyczyszczenie pliku
            StreamWriter FileWrite = new StreamWriter(FileW);
            int index = 0;
            foreach (ChocoProducts c in list) //zapisanie poszczeg�lnych wpis�w z listy oddzielaj�c je '"'
            {
                if (list[index] is Chocolate)
                {
                    FileWrite.Write("0\"");
                }
                else if (list[index] is CandyBar)
                {
                    FileWrite.Write("1\"");
                }
                else if (list[index] is Other)
                {
                    FileWrite.Write("2\"");
                }
                FileWrite.Write(c.getName() + "\"");
                FileWrite.Write(c.getProducer() + "\"");
                FileWrite.Write(c.getPrice() + "\"");
                FileWrite.Write(c.getWeight() + "\"");
                if (list[index] is Chocolate)
                {
                    Chocolate choco = (Chocolate)c;
                    FileWrite.Write(choco.getKind() + "\"");
                }
                else if (list[index] is CandyBar)
                {
                    CandyBar bar = (CandyBar)c;
                    FileWrite.Write(bar.getDescription() + "\"");
                }
                else if (list[index] is Other)
                {
                    Other ot = (Other)c;
                    FileWrite.WriteLine(ot.getPiecesInPack() + "\"");
                }

                index++;
            }
            MessageBox.Show("Plik zosta� poprawnie zapisany");
            FileWrite.Close();
            this.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = comboBox1.SelectedIndex;
            listBox1.Items.Clear();
            textBoxProducer.Text = "";
            textBoxDescription.Text = "";
            textBoxKind.Text="";
            textBoxPiecesInPack.Text = "";
            textBoxPrice.Text = "";
            textBoxWeight.Text = "";
            switch (index)
            {
                case 0:
                    {
                        foreach (ChocoShop i in list)
                        {
                            if (i.GetType().ToString() == "czekolada.Chocolate")
                            {
                                Chocolate choco = (Chocolate)i;
                                listBox1.Items.Add(choco.getName());  
                            }
                        }
                        break;
                    }
                case 1:
                    {
                        foreach (ChocoShop i in list)
                        {
                            if (i.GetType().ToString() == "czekolada.CandyBar")
                            {
                                CandyBar bar = (CandyBar)i;
                                listBox1.Items.Add(bar.getName());
                            }
                        }
                        break;
                    }
                case 2:
                    {
                        foreach (ChocoShop i in list)
                        {
                            if (i.GetType().ToString() == "czekolada.Other")
                            {
                                Other ot = (Other)i;
                                listBox1.Items.Add(ot.getName());
                            }
                        }
                        break;
                    }
                case 3:
                    {
                        foreach (ChocoShop i in list)
                        {
                            listBox1.Items.Add(i.getName());
                        }
                        break;
                    }
            }
        }

        private void Menu_EnabledChanged(object sender, EventArgs e)
        {
            Add form = new Add();
            form.AddItemCallback += new Add.AddChoco(this.AddItemCallback2);
            Edit formEdit = new Edit();
            formEdit.EditItemCallback += new Edit.EditChoco(this.okno1_EditItemCallback);
        }

        private void Menu_Leave(object sender, EventArgs e)
        {
            string path = Application.StartupPath;
            string MyPath = @path + "\\dane\\choco.txt";
            DirectoryInfo dir = new DirectoryInfo(@path + "\\dane");
            FileInfo file = new FileInfo(@path + "\\dane\\choco.txt");
            if (dir.Exists == true) //sprawdzenie czy plik i katalog istniej�
            {
                if (file.Exists == false)
                {
                    file.Create();
                }
            }
            else
            {
                dir.Create();
            }
            FileStream FileW = new FileStream(MyPath, FileMode.Truncate); //otworzenie oraz wyczyszczenie pliku
            StreamWriter FileWrite = new StreamWriter(FileW);
            int index = 0;
            foreach (ChocoProducts c in list) //zapisanie poszczeg�lnych wpis�w z listy oddzielaj�c je '"'
            {
                if (list[index] is Chocolate)
                {
                    FileWrite.Write("0\"");
                }
                else if (list[index] is CandyBar)
                {
                    FileWrite.Write("1\"");
                }
                else if (list[index] is Other)
                {
                    FileWrite.Write("2\"");
                }
                FileWrite.Write(c.getName() + "\"");
                FileWrite.Write(c.getProducer() + "\"");
                FileWrite.Write(c.getPrice() + "\"");
                FileWrite.Write(c.getWeight() + "\"");
                if (list[index] is Chocolate)
                {
                    Chocolate choco = (Chocolate)c;
                    FileWrite.Write(choco.getKind() + "\"");
                }
                else if (list[index] is CandyBar)
                {
                    CandyBar bar = (CandyBar)c;
                    FileWrite.Write(bar.getDescription() + "\"");
                }
                else if (list[index] is Other)
                {
                    Other ot = (Other)c;
                    FileWrite.WriteLine(ot.getPiecesInPack() + "\"");
                }

                index++;
            }
            MessageBox.Show("Plik zosta� poprawnie zapisany");
            FileWrite.Close();
        }

        private void Menu_Load(object sender, EventArgs e)
        {

        }


    }
}