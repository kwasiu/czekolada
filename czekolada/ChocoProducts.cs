using System;
using System.Collections.Generic;
using System.Text;

namespace czekolada
{
     public class ChocoProducts:ChocoShop
    {
        protected string name;
        protected string producer;
        protected int weight;
        protected double price;

        public ChocoProducts()
        {
            name = "";
            producer = "";
            weight = -1;
            price = -1;
        }

        public ChocoProducts(string name, string producer, int weight, double price)
        {
            this.name = name;
            this.producer = producer;
            this.weight = weight;
            this.price = price;
        }

        public string getName()
        {
            return name;
        }
        public void setName(string name)
        {
            this.name = name;
        }
        public string getProducer()
        {
            return producer;
        }
        public void setProducer(string producer)
        {
            this.producer = producer;
        }
        public int getWeight()
        {
            return weight;
        }
        public void setWeight(int weight)
        {
            this.weight = weight;
        }
        public double getPrice()
        {
            return price;
        }
        public void setPrice(double price)
        {
            this.price = price;
        }
    }
}
