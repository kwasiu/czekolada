﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace czekolada
{
    public partial class Edit : Form
    {
        ChocoProducts product = new ChocoProducts();
        int index = 0;
        int type;
        public delegate void EditChoco(int index, ChocoProducts product);
        public event EditChoco EditItemCallback;

        public Edit()
        {
            InitializeComponent();
        }

        public Edit(int index, ChocoProducts product)
        {
            InitializeComponent();
            this.index = index;
            this.product = product;
        }

        private void Edit_Load(object sender, EventArgs e)
        {
            textBoxName.Text = product.getName();
            textBoxProducer.Text = product.getProducer();
            textBoxPrice.Text = product.getPrice().ToString();
            textBoxWeight.Text = product.getWeight().ToString();
            if (product is Chocolate)
            {
                type = 1;
                Chocolate choco = (Chocolate)product;
                textBoxKind.Text = choco.getKind();
                textBoxPiecesInPack.Enabled = false;
                textBoxDescription.Enabled = false;
            }
            else
            {
                if (product is CandyBar)
                {
                    type = 2;
                    CandyBar bar = (CandyBar)product;
                    textBoxDescription.Text = bar.getDescription();
                    textBoxPiecesInPack.Enabled = false;
                    textBoxKind.Enabled = false;
                }
                else
                {
                    type = 3;
                    Other ot = (Other)product;
                    textBoxPiecesInPack.Text = ot.getPiecesInPack().ToString();
                    textBoxKind.Enabled = false;
                    textBoxDescription.Enabled = false;
                }
            }
        }

        private DialogResult weryfikacjaDanych()
        {
            int licznikBledow = 0;
            if (textBoxName.Text == "")
            {
                licznikBledow++;
                MessageBox.Show("Wpisz nazwę");
            }
            if (textBoxProducer.Text == "")
            {
                licznikBledow++;
                MessageBox.Show("Wpisz nazwę producenta");
            }
            if (textBoxPrice.Text == "")
            {
                licznikBledow++;
                MessageBox.Show("Wpisz cenę");
            }
            double price;
            if (!(Double.TryParse(textBoxPrice.Text, out price)))
            {
                licznikBledow++;
                MessageBox.Show("Wpisz poprawnie cenę");
            }
            if (price <= 0)
            {
                licznikBledow++;
                MessageBox.Show("Wartość ceny jest liczbą dodatnią");
            }
            if (textBoxWeight.Text == "")
            {
                licznikBledow++;
                MessageBox.Show("Wpisz wagę");
            }
            int weight;
            if (!(Int32.TryParse(textBoxWeight.Text, out weight)))
            {
                licznikBledow++;
                MessageBox.Show("Wpisz poprawnie wagę");
            }
            if (weight <= 0)
            {
                licznikBledow++;
                MessageBox.Show("Wartość wagi jest liczbą dodatnią");
            }
            switch(type)
            {
                case 1:
                {
                    if(textBoxKind.Text=="")
                    {
                        licznikBledow++;
                        MessageBox.Show("Wpisz rodzaj");
                    }
                    break;
                }
                case 2:
                {
                    if (textBoxDescription.Text == "")
                    {
                        licznikBledow++;
                        MessageBox.Show("Uzupełnij opis");
                    }
                    break;
                }
                case 3:
                {
                    if (textBoxPiecesInPack.Text == "")
                    {
                        licznikBledow++;
                        MessageBox.Show("Uzupełnij ilość sztuk w paczce");
                    }
                    int piecesInPack;
                    if (!(Int32.TryParse(textBoxPiecesInPack.Text, out piecesInPack)))
                    {
                        licznikBledow++;
                        MessageBox.Show("Wpisz poprawnie ilość sztuk w paczce");
                    }
                    break;
                }

            }
            if (licznikBledow == 0)
            {
                return DialogResult.OK;
            }
            else
            {
                return DialogResult.Cancel;
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            product.setName(textBoxName.Text);
            product.setProducer(textBoxProducer.Text);
            product.setPrice(Convert.ToDouble(textBoxPrice.Text));
            product.setWeight(Convert.ToInt32(textBoxWeight.Text));
            if (product is Chocolate)
            {
                type = 1;
                Chocolate choco = (Chocolate)product;
                choco.setKind(textBoxKind.Text);
                textBoxPiecesInPack.Enabled = false;
                textBoxDescription.Enabled = false;
                EditItemCallback(index, product);
            }
            else
            {
                if (product is CandyBar)
                {
                    type = 2;
                    CandyBar bar = (CandyBar)product;
                    bar.setDescription(textBoxDescription.Text);
                    textBoxPiecesInPack.Enabled = false;
                    textBoxKind.Enabled = false;
                    EditItemCallback(index, product);
                }
                else
                {
                    type = 3;
                    Other ot = (Other)product;
                    ot.setPiecesInPack(Convert.ToInt32(textBoxPiecesInPack.Text));
                    textBoxKind.Enabled = false;
                    textBoxDescription.Enabled = false;
                    EditItemCallback(index, product);
                }
            }
            this.Close();
        }
    }
}
            

