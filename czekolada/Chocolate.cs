using System;
using System.Collections.Generic;
using System.Text;

namespace czekolada
{
    class Chocolate : ChocoProducts, ChocoShop
    {
        protected string kind;

        public Chocolate()
            : base()
        {
            kind="";
        }

        public Chocolate(string name, string producer, int weight, double price,string kind)
            :base(name, producer, weight, price)
        {
            this.kind=kind;
        }

        public string getKind()
        {
            return kind;
        }
        public void setKind(string kind)
        {
            //dopisa� sprawdzanie poprawnosci danych
            this.kind = kind;
        }
    }
}
